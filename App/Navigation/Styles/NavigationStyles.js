import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  header: {
    backgroundColor:'#ff585b'
  },
  label: {
  	color: 'red',
  	textAlign: 'center'
  }
})
