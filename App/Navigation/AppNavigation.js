import React, {Component} from 'react';
import { TabNavigator, TabBarBottom, StackNavigator } from 'react-navigation'
import TeamScreen from '../Containers/TeamScreen'
import ProfileScreen from '../Containers/ProfileScreen'
import PokeScreen from '../Containers/PokeScreen'
import { Colors, Metrics } from '../Themes/'

import Ionicons from 'react-native-vector-icons/Ionicons';

import Styles from './Styles/NavigationStyles'

const PrimaryNav = TabNavigator({
  Dashboard: {
    screen: StackNavigator({
      Pokedex: { 
        screen: PokeScreen,
        navigationOptions: {
          title: 'PokéDex', 
          headerStyle: Styles.header,
          headerTitleStyle: {
            color: 'white',
            alignSelf:'center',
            textAlign: 'center',
            width: '90%',
          },
          headerTintColor: {
            color: Colors.white
          },
          titleStyle:{
            textAlign: 'center'
          }
        }
      },
      ProfileScreen: {
        screen: ProfileScreen,
        navigationOptions: ({ navigation }) => (
              {
                  title: 'PokéProfile',
                  headerStyle: Styles.header,
                  headerTitleStyle: {
                    color: Colors.white,
                    alignSelf:'center',
                    textAlign: 'center',
                    width: '75%',
                  },
                  headerBackTitleStyle: {
                    color: Colors.white
                  },
                  headerTintColor:Colors.white
              })
      }
    }),
  },
  Team: {
    screen: TeamScreen,
    navigationOptions: {
      title: 'My Team',
      header:{
        visible: true
      },
      headerStyle: Styles.header,
      headerTitleStyle: {
        color: 'white',
        alignSelf:'center',
        textAlign: 'center',
        width: '90%',
      },
      headerTintColor: {
        color: Colors.white
      },
      titleStyle:{
        textAlign: 'center'
      }
    }
  }
},{
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, tintColor }) => {
      const { routeName } = navigation.state;
      let iconName;
      if (routeName === 'Dashboard') {
        iconName = `ios-tablet-portrait${focused ? '' : '-outline'}`;
      } else if (routeName === 'Team') {
        iconName = `ios-apps${focused ? '' : '-outline'}`;
      }
      // You can return any component that you like here! We usually use an
      // icon component from react-native-vector-icons
      return <Ionicons name={iconName} size={25} color={tintColor} />;
    },
  }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
      style : {
       margin: 10,
      }
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    initialRouteName: 'Dashboard'
})


export default PrimaryNav
