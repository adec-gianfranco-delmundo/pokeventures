import { takeLatest, all, takeEvery } from 'redux-saga/effects'
import API from '../Services/Api'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { PokemonTypes } from '../Redux/PokemonRedux'
import { ProfileTypes } from '../Redux/ProfileRedux'

/* ------------- Sagas ------------- */
import { startup } from './StartupSagas'
import { getPokemon, filterPokemon, addPokemonToTeam } from './PokemonSagas'
import { getProfile } from './ProfileSagas'

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const api = DebugConfig.useFixtures ? FixtureAPI : API.create()

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    // some sagas only receive an action
    takeLatest(StartupTypes.STARTUP, startup),

    // some sagas receive extra parameters in addition to an action
    takeLatest(PokemonTypes.GET_POKEMONS, getPokemon, api),
    takeLatest(PokemonTypes.PERFORM_SEARCH, filterPokemon),
  	// takeLatest(PokemonTypes.ADD_POKEMON_SUCCESS, addPokemonToTeam),
    
    takeLatest(ProfileTypes.PROFILE_REQUEST, getProfile, api)
   ])
}
