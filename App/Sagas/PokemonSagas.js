import { call, put } from 'redux-saga/effects'
import { path } from 'ramda'
import PokemonActions from '../Redux/PokemonRedux'
import { getPokemonList } from './selectors'

export function * getPokemon (api) {
  // make the call to the api
  const response = yield call(api.getPokemons)

  if (response.ok) {
    const pokemon = response.data;
    // do data conversion here if needed
    yield put(PokemonActions.getPokemonSuccess(pokemon.results))
  } else {
    yield put(PokemonActions.getPokemonFailure())
  }
}

export function * filterPokemon (searchTerm) {
	// const pokeList = yield select(getPokemonList)
	yield put(PokemonActions.performSearch(searchTerm))
}

export function * addPokemonToTeam (pokedata) {
  yield put(PokemonActions.addPokemonSuccess(pokedata))
}

export function * resetPokemon () {
  yield put(PokemonActions.resetPokemon())
}
