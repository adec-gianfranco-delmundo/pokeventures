import { StyleSheet } from 'react-native'
import { ApplicationStyles } from '../../Themes/'

export default StyleSheet.create({
  ...ApplicationStyles.screen,
  main: {
  	backgroundColor: '#ff585b',
  	flex: 1,
  	paddingTop: 40
  },
  container: {
  	flex: 1,
  	backgroundColor: '#ff585b'
  },
  title:{ 
  	color:"#3f6269",
  	fontWeight:'600'
  },
  val:{
  	color:"#999"
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
})
