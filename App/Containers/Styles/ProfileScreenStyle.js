import { StyleSheet } from 'react-native'
import { ApplicationStyles, Colors } from '../../Themes/'

export default StyleSheet.create({
  rectanglebit : {
  	height:56,
  	borderRadius:4,
  	flexDirection:'row',
  	borderColor:'#e3e3e3',
  	margin:20,
  	backgroundColor: Colors.white,
  	borderWidth: 1

  },
  columnbit:{
  	flex:1,
  	alignItems:'center',
  	justifyContent:'space-around'
  },
  title:{ 
  	color:"#3f6269",
  	fontWeight:'600'
  },
  val:{
  	color:"#999"
  },
  actionButtonIcon: {
    fontSize: 20,
    height: 22,
    color: 'white',
  },
})
