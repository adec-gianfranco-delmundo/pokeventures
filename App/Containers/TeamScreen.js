import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, FlatList, Image, Platform, InteractionManager } from 'react-native'
import { connect } from 'react-redux'
import { Colors, Metrics, Images } from '../Themes/'
import ViewOverflow from 'react-native-view-overflow';
import ActionButton from 'react-native-action-button';
import Ionicon from 'react-native-vector-icons/Ionicons';
import PokemonActions from '../Redux/PokemonRedux'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/TeamScreenStyle'

class TeamScreen extends Component {
  renderPokemon (rowData) {
    console.tron.log(rowData)
    return (
        <View style={{position: 'relative'}}>
            <ViewOverflow>
            <View style={{alignItems: 'center', justifyContent: 'center',borderWidth: 20, borderColor: Colors.white, zIndex: 1, flex: 1, flexDirection:'row', backgroundColor: rowData.item.color, elevation: 1, margin: 10, borderRadius: (Metrics.screenHeight / 6) / 2, height: Metrics.screenHeight / 6,alignItems: 'stretch'}}>
              <View style={{flex: 3, marginLeft: '10%', marginTop: '5%'}}>
                <Text style={{zIndex: 1, flex: 1, color: Colors.white, backgroundColor: 'transparent', fontSize: 20}}>{rowData.item.name}</Text>
              </View>
               <Image
                    source={{uri: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+rowData.item.id+'.png'}}
                    resizeMode='stretch' 
                    style={{
                      width: 140,
                      height: 140,
                      zIndex: 1,
                      position: 'absolute',
                      top: Platform.OS == 'ios' ? '-50%' : '-65%',
                      right: -10
                    }}
                />
            </View>
            </ViewOverflow>
        </View>
    )
  }

  renderEmpty(){
    return(
      <View style={{flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{color: Colors.white, fontSize: 30, textAlign: 'center', padding: 40}}>Press the Pokéball to BUILD your Team.</Text>
        <Image source={Images.profile} resizeMode='contain' style={{top: -100, width: Metrics.screenWidth-150, height: Metrics.screenHeight - 100}}/>
      </View>
    )
  }

  keyExtractor = (item, index) => index;

  render () {
    return (
          <View style={styles.main}>
            <FlatList
              ref={ref => this.flatList = ref}
              shouldItemUpdate={(props,nextProps)=>
                { 
                  // return props.item!==nextProps.item
                }
              }

              cellRendererComponent={ViewOverflow}  
              onContentSizeChange={() => 
                InteractionManager.runAfterInteractions(() => {
                  this.flatList.scrollToOffset({x: 0, y: 0, animated: true})
                })
              }
              ListEmptyComponent={this.renderEmpty()}
              // searchProperty={"name"}
              // searchTerm={this.state.searchTerm}
              keyExtractor={this.keyExtractor}
              style = {{flex:1}}
              data={this.props.team}
              renderItem={(rowData) => this.renderPokemon(rowData)}
              // onEndReached={this.handleLoadMore}
              onEndReachedThreshold={0.8}
              initialNumToRender={5}
              // ListHeaderComponent={this.renderHeader}
            />
            <ActionButton buttonColor="white"
              renderIcon={() => <Image source={Images.pokeball} style={{height:40, width: 40}}></Image>}
            >
              <ActionButton.Item buttonColor='#1abc9c' title="Add Pokemon" onPress={() => this.props.navigation.navigate('Dashboard')}>
                <Ionicon name="md-add" style={styles.actionButtonIcon} />
              </ActionButton.Item>
              <ActionButton.Item buttonColor='#F03434' title="Reset Team" onPress={() => { 
                this.props.resetPokemon()
                this.flatList.scrollToOffset({x: 0, y: 0, animated: true})

              }}>
                <Ionicon name="md-trash" style={styles.actionButtonIcon} />
              </ActionButton.Item>
            </ActionButton>
          </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    team : state.pokemon.team
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetPokemon: () => dispatch(PokemonActions.resetPokemon()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamScreen)
