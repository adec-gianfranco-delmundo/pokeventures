import React, { Component } from 'react';
import {
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Navigator,
  FlatList,
  Dimensions,
  TextInput,
  ActivityIndicator,
  View,
  Platform
} from 'react-native';

import { SearchBar } from 'react-native-elements'
import PokemonActions from '../Redux/PokemonRedux'
import { connect } from 'react-redux'
import { Colors, Metrics, Images } from '../Themes/'

var {height, width} = Dimensions.get('window');
import Icon from 'react-native-vector-icons/MaterialIcons';
import Nav from '../Components/PokemonNav'
import Footer from "../Components/PokeballFoot"
import * as Animatable from 'react-native-animatable';


// This is a dope link for pokedex data https://pokeapi.co/

class Pokedex extends Component {
  constructor(props){
    super(props)

      this.state = {
        dataSource: [],
        selectedName: '',
        selectedId: 1,
        searchInput: '',
        pokemonList: [{
          name: '',
          url: '/1/'
        }],
        name: '',
        number: 0
      }
 
  }

  componentDidMount() {
    this.props.getPokemon()
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.pokedata != this.props.pokedata) {
      this.setState({pokemonList: this.props.pokedata})
      this.selectPokemon(this.state.pokemonList[0]);
    }
  }

  pad (str, max) {
    str = str.toString();
    return str.length < max ? this.pad("0" + str, max) : str;
  }

  selectPokemon(val){
    let url = val.url.split('/'),
        pokeid = url[url.length - 2]

    this.setState({selectedName: val.name.toUpperCase(), selectedId: pokeid, name: val.name.toUpperCase(), number: this.pad(pokeid, 6)})
    this.refs.view.swing(800)
  }



  balls(x){
    let url = x.item.url.split('/'),
        pokeid = url[url.length - 2]

    if(x.item.name.toUpperCase() !== this.state.selectedName){
        return(
          <Animatable.View useNativeDriver={true} animation="fadeInDownBig">
            <View style={{flexDirection:'row', justifyContent:'flex-end', alignItems:'center', margin:10}}>
              <Text style={{backgroundColor: 'transparent', marginTop:55, fontSize:11, color:'#bbb8b6'}}>#{this.pad(pokeid,4)}  </Text>
              <Text style={{backgroundColor: 'transparent', marginTop:55, color:'#555', fontWeight:'700', fontSize:11}}>{x.item.name.toUpperCase()}  </Text>
              <View style={{height:80, alignItems:'center', justifyContent:'center'}} >
                <View style={styles.line} />
                <TouchableOpacity onPress = {() => this.selectPokemon(x.item)}>
                  <Image source={require('../Images/pokeOutline.png')} resizeMode="stretch" style={{marginTop:(Platform.OS !== 'ios' ? -4 : -6),height:50, width:50}} />
                </TouchableOpacity>
              </View>
            </View>
          </Animatable.View>
          )}
        else{
          return(
          <Animatable.View useNativeDriver={true} animation="fadeInDownBig">
            <View style={{flexDirection:'row', justifyContent:'flex-end', alignItems:'center', margin:10}}>
              <Text style={{backgroundColor: 'transparent', marginTop:55, fontSize:11, color:'#bbb8b6'}}>#{this.pad(pokeid,4)}  </Text>
              <Text style={{backgroundColor: 'transparent', marginTop:55, color:'#555', fontWeight:'700', fontSize:11}}>{x.item.name.toUpperCase()}  </Text>
              <View style={{height:80, alignItems:'center', justifyContent:'center'}} >
              <View style={styles.line} />
                <TouchableOpacity>
                  <Image source={require('../Images/greatfile.png')} resizeMode="stretch" style={{marginTop:(Platform.OS !== 'ios' ? -3 : -6),height:50, width:50}} />
                </TouchableOpacity>
              </View>
            </View>
          </Animatable.View>
          )}
        
  }

  keyExtractor = (item, index) => item.name;
  
  setSearchText(event){
    searchText = event.nativeEvent.text;
    data       = this.props.pokedata;
    searchText = searchText.trim().toLowerCase();
    
    data = data.filter(l => {
     return l.name.toLowerCase().match( searchText );
    });

    this.setState({
     pokemonList : data
    });
  }

  renderHeader = () => {
    return <SearchBar onChange={this.setSearchText.bind(this)} placeholder="Type Here..." lightTheme round />;
  }

  renderFooter = () => {
    if(this.state.pokemonList.length == 0)
      return (
        <View
          style={{
            paddingVertical: 20,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <Text>No Pokemon Found</Text>
          </View>
      )

    return (
        <View
          style={{
            paddingVertical: 20,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <ActivityIndicator animating />
          <Animatable.Text useNativeDriver={true} iterationCount="infinite" animation="flipOutX">Connecting...</Animatable.Text>
        </View>
      );

  }

  render(){
    return(
      <View style={{flex:1, backgroundColor:'#e9e9e9'}}>
        <View>
          {/*<TextInput
            style={{height: 40, backgroundColor: 'white', borderColor: 'gray', borderWidth: 1}}
            onChangeText={(text) => this.updateFilter(text)}
            value={this.state.searchInput}
          />*/}
          { this.renderHeader() }
        </View>
        <View style={{flex:1, flexDirection:'row'}}>
          <View style={{alignSelf: 'center'}}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ProfileScreen',{id: this.state.selectedId})}>
              <Animatable.Image 
                // loadingIndicatorSource={() => <Image source={Images.pokeball} />}
                defaultSource={Images.pokeball}
                ref="view" useNativeDriver={true} animation="swing" source={{uri: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+ this.state.selectedId +'.png'}} resizeMode='contain' style={{height:Metrics.screenWidth / 2, width:Metrics.screenWidth / 2, margin: 0}} />
            </TouchableOpacity>
            <View style={{top: -40, flex: -1, backgroundColor:this.state.color, padding:5, borderRadius:5, margin:10, marginBottom:0}}>
              <Text style={{backgroundColor: 'transparent',textAlign: 'center', color:'tomato', fontSize:20, fontWeight:'600'}}>{this.state.name}</Text>
            </View>
            <View style={{top: -40, flex: -1, backgroundColor:"#c3c3c3", padding:5, borderRadius:5, margin:10 }}>
              <Text style={{textAlign: 'center', color:'#fff', fontSize:14, fontWeight:'600'}}>#{this.state.number}</Text>
            </View>
          </View>
          { <FlatList
            shouldItemUpdate={(props,nextProps)=>
              { 
                return props.item!==nextProps.item
              }
            }
            // searchProperty={"name"}
            // searchTerm={this.state.searchTerm}
            keyExtractor={this.keyExtractor}
            style = {{flex:1}}
            data={this.state.pokemonList}
            renderItem={(rowData) => this.balls(rowData)}
            // onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.8}
            initialNumToRender={5}
            ListFooterComponent={this.renderFooter}
            // ListHeaderComponent={this.renderHeader}
          />
        }
        </View>
      </View>
      )
  }
}

const styles = StyleSheet.create({
  line:{height:60, width:2, backgroundColor:'#bababb' },
 
});

const mapStateToProps = (state) => {
  return {
    pokedata: state.pokemon.list,
    fetching: state.pokemon.fetching
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPokemon: () => dispatch(PokemonActions.getPokemons()),
    performSearch: (searchTerm) => dispatch(PokemonActions.performSearch(searchTerm)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Pokedex)
