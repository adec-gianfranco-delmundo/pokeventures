import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Navigator,
  ScrollView,
  View,
  ActivityIndicator,
  Platform,
  WebView,
  InteractionManager,
  Alert
} from 'react-native';
import Footer from "../Components/PokeballFoot"
import Icon from 'react-native-vector-icons/MaterialIcons';
import * as Animatable from 'react-native-animatable';
import { Colors, Metrics, Images } from '../Themes/'
import ProfileActions from '../Redux/ProfileRedux'
import PokemonActions from '../Redux/PokemonRedux'
import LottieView from 'lottie-react-native';
import ActionButton from 'react-native-action-button';
import Ionicon from 'react-native-vector-icons/Ionicons';
import styles from './Styles/ProfileScreenStyle'

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

class ProfileScreen extends Component {
  constructor(props){
    super(props)

    this.state = {
      background : 'default'
    }
 
  }

  componentDidMount(){
    this.props.getProfile(this.props.navigation.state.params.id)
    // InteractionManager.runAfterInteractions(() => {
    //   this.props.addToTeam(
    //       {
    //         name: 'PIKACHU',
    //         color: 'red'
    //       }
    //     );  
    // })

  }
  
  componentWillReceiveProps(nextProps){
    if(nextProps.profile != this.props.profile){
      // Doesn't compliment the change in header color. LOL
      // InteractionManager.runAfterInteractions(() => {
      //   this.props.navigation.setParams({color: (this.props.profile ? Colors[this.props.profile.types[0].type.name] : '#ff585b')})
      // });
    }
  }

  pad (str, max) {
    str = str.toString();
    return str.length < max ? this.pad("0" + str, max) : str;
  }

  addPokemon(){
    InteractionManager.runAfterInteractions(() => {
      if(this.props.team.length < 6){
        this.props.addToTeam(
          {
            id: this.props.navigation.state.params.id,
            name: this.props.profile.name.capitalize(),
            color: Colors[this.props.profile.types[0].type.name]
          }
        );

        this.props.navigation.navigate('Team')   
      } else {
        Alert.alert(
          'Unable to Add More',
          'Maximum of 6 Pokemons per Team only.',
          [
             {text: 'OK', onPress: () => console.log('OK Pressed')},
          ],
          { cancelable: false }
        )
      }
    });


    console.tron.log('POKEMON ADDED!')
  }

  render(){
    return(
      <View style={{flex: 1}}>
        <View style={{flex:1, justifyContent:'flex-end'}}>
        </View>
        <Image source={Images[this.props.profile ? this.props.profile.types[0].type.name : 'default']} resizeMode='stretch' style={{position: 'absolute', width: '100%', height: '100%'}} />  
        <View style={{position:'absolute', marginTop: 50, zIndex: 3, alignSelf:'center', borderRadius: Metrics.screenWidth / 4, width: Metrics.screenWidth / 2, height: Metrics.screenWidth / 2, overflow: 'hidden'}}>
          <Image borderRadius={Metrics.screenWidth/4} source={{uri: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/'+ this.props.navigation.state.params.id +'.png'}} resizeMode="cover" style={{borderWidth: 14, borderColor: Colors.white, backgroundColor: Colors.steel, width:Metrics.screenWidth/2, height: Metrics.screenWidth/2, alignSelf:'center', zIndex: 3}} />
        </View>
        <View style={{ shadowColor: '#000', shadowOffset: { width: 0, height: 2 }, shadowOpacity: 0.8, shadowRadius: 2, flex:3, margin: 10, marginBottom: 0, borderTopLeftRadius: 6, borderTopRightRadius: 6, backgroundColor:'rgba(255,255,255,0.9)', position: 'relative'}}>
          <View style={{flexDirection:'row', marginTop: Platform.os == 'ios' ? 110 : 120, justifyContent:'center'}}>
            <Text style={{fontSize:18, color:'#a6a6a6'}}>#{this.props.profile ? this.pad(this.props.profile.id,6): '000000'}  </Text>
            <Text style={{fontSize: 18, fontWeight:"600", color:'#3f6269'}}>{this.props.profile ? this.props.profile.name.capitalize() : 'name'}</Text>
          </View>
          <View style={styles.rectanglebit}>
            <View style={styles.columnbit}>
              <Text style={styles.title}>
              Type
              </Text>
              <View style={{flexDirection: 'row'}}>
                {
                  this.props.profile ? this.props.profile.types.map(function(num, i) {
                    return (
                      <View key={i} style={{backgroundColor: Colors[num.type.name], borderRadius: 2, padding: 2, marginRight: 3, left: 3}}>
                        <Text style={{color: Colors.white, textAlign: 'center'}}>{ num.type.name.capitalize() }</Text>
                      </View>
                    )
                  }) : null
                }
              </View>
            </View>
            <View style={styles.columnbit}>
              <Text style={styles.title}>
              Weight
              </Text>
              <Text style={styles.val}>
              {this.props.profile ? this.props.profile.weight : '0'}kg
              </Text>
            </View>
            <View style={styles.columnbit}>
              <Text style={styles.title}>
              Height
              </Text>
              <Text style={styles.val}>
              {this.props.profile ? this.props.profile.height : '0'}cm
              </Text>
            </View>
          </View>
          <ScrollView contentContainerStyle={{margin:20, marginTop:5}}>
            <Text style={{color:'#999', flex: 1, flexWrap: 'wrap', lineHeight:22, fontWeight:'500', fontSize:14, textAlign: 'justify'}}>
              {this.props.desc ? this.props.desc.flavor_text_entries[1].flavor_text : 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque quia quam laudantium deleniti quidem mollitia quaerat sit dolores reiciendis, quae maxime earum officia recusandae perspiciatis dolorem, dignissimos! Velit, error, recusandae.'}
            </Text>
          </ScrollView>
        </View>
        {(this.props.fetching) && <View style={{flex: 1, justifyContent: 'center', zIndex: 3, alignItems: 'center', position: 'absolute', width: '100%', height: '100%', backgroundColor: 'white', opacity: 0.9}}>
                  <LottieView 
                    source={require('../Images/preloader.json')}  
                    ref={animation => {
                      if (animation) animation.play()
                    }}
                  />
                </View>}
        { /*ACTION BUTTON START*/ }
        <ActionButton buttonColor="white"
          renderIcon={() => <Image source={Images.pokeball} style={{height:40, width: 40}}></Image>}
        >
          <ActionButton.Item buttonColor='#1abc9c' title="Add to Team" onPress={() => this.addPokemon()}>
            <Ionicon name="md-add" style={styles.actionButtonIcon} />
          </ActionButton.Item>
          <ActionButton.Item buttonColor='#3498db' title="Go to Team" onPress={() => { this.props.navigation.navigate('Team') }}>
            <Ionicon name="md-grid" style={styles.actionButtonIcon} />
          </ActionButton.Item>
        </ActionButton>
      </View>
      )
  }
}


// const styles = StyleSheet.create({

 
// });

const mapStateToProps = (state) => {
  return {
    profile: state.profile.payload,
    desc: state.profile.payload2,
    fetching: state.profile.fetching,
    team: state.pokemon.team
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getProfile: (id) => dispatch(ProfileActions.profileRequest(id)),
    addToTeam: (pokedata) => dispatch(PokemonActions.addPokemon(pokedata)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen)
