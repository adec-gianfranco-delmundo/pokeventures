import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'
import { orderBy } from 'lodash'
import { filter, where, contains } from 'ramda'
import { startsWith } from 'ramdasauce'


/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  getPokemons: null,
  getPokemonSuccess: ['pokemon'],
  getPokemonFailure: null,
  performSearch: ['searchTerm'],
  addPokemon: ['pokedata'],
  resetPokemon: null
});


export const PokemonTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  list: [{
    "id": 1,
    "num": "001",
    "name": "BULBASAUR",
    'type': "poison",
    'color': '#a529ae',
    "selected": true,
    "url": '/1/'
  }],
  team: []
})

/* ------------- Reducers ------------- */

export const getPokemons = (state) => {
  return state.merge({ fetching: true})
}

export const getPokemonSuccess = (state, { pokemon }) => {
  return state.merge({ fetching: false, list: pokemon })
}

export const getPokemonFailure = (state) =>{
  return state.merge({ fetching: false })
}

export const performSearch = (state, { searchTerm }) => {
  // console.tron.log(state)
  // const results = filter(where({name: contains(searchTerm)}), state.list)
  
  return INITIAL_STATE;
  // return state.merge({ fetching: true, list: results })
}

export const addPokemon = (state, { pokedata }) => {
  return state.merge({ fetching: false, team: [...state.team, pokedata] })
}

export const resetPokemon = () => {
  return INITIAL_STATE;
}


export const reducer = createReducer(INITIAL_STATE, {
  [Types.GET_POKEMONS]: getPokemons,
  [Types.GET_POKEMON_SUCCESS]: getPokemonSuccess,
  [Types.GET_POKEMON_FAILURE]: getPokemonFailure,
  [Types.PERFORM_SEARCH]: performSearch,
  [Types.ADD_POKEMON]: addPokemon,
  [Types.RESET_POKEMON]: resetPokemon,
})
